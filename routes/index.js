var express = require('express');
var router = express.Router();
const base = require('../controllers/baseControllers')

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });


router.get('/', base.index)
router.post('/sum', base.sum)

module.exports = router;
