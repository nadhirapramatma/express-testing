const request = require('supertest')
const app = require('../app')

describe('GET /', () => {
  test('Return status: 200 and a hello world message', done => {
    request(app).get('/index').then(res => {
      expect(res.statusCode).toBe(200)
      expect(res.body).toHaveProperty('status')
      expect(res.body).toHaveProperty('message')
      expect(res.body.status).toBe(true)
      expect(res.body.message).toEqual("Hi, this is an express testing app")
      done()
    })
  })
})

// describe('POST /', () => {
//   test('Return status: 200 and a hello world message', done => {
//     const request = {
//       x: 3,
//       y: 2
//     }
//     request(app).post('/index').then(res => {
//       expect(res.statusCode).toBe(200)
//       expect(res.body).toHaveProperty('status')
//       expect(res.body).toHaveProperty('message')
//       expect(res.body).toHaveProperty('data')
//       expect(res.body.status).toBe(true)
//       expect(res.body.message).toEqual("Hello World!")
//       done()
//     })
//   })
// })